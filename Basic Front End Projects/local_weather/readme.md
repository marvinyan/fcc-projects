Show the Local Weather
===
**[Project demo](http://codepen.io/marvinyan/full/KpLEbM)**

[Link to FreeCodeCamp challenge page](http://freecodecamp.com/challenges/zipline-show-the-local-weather)

Objective
---

Build a CodePen.io app that successfully reverse-engineers  [this](http://codepen.io/AdventureBear/full/yNBJRj).

User Story: As a user, I can see the weather in my current location.

External resources
---

Location API from [freegeoip.net](https://freegeoip.net).

Weather API from [OpenWeatherMap](http://openweathermap.org/).
