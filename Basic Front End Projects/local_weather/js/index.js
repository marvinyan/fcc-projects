$(document).ready(function() {
  function getLocationAndWeather() {
    // Get location
    $.get('http://ipinfo.io/json', function(location) {
      // Get weather using location data
      $.get('http://api.openweathermap.org/data/2.5/weather?zip=' + location.postal + ',' + location.country + '&units=imperial&appid=6419b35e7dcf3975b41fa076fbbff5e3', function(weather) {
        // Update display
        $("#location").text(weather.name);
        $("#icon").prop("src", "http://openweathermap.org/img/w/" + weather.weather[0].icon + ".png")
        $("#temp").text(Math.round(weather.main.temp) + " °F");
        $("#condition").text(weather.weather[0].description);
        $("#wind").text(weather.wind.speed + " mph " + convertDegreesToDirection(weather.wind.deg));
      })
    });
  }

  // 0 is N, clockwise traversal
  function convertDegreesToDirection(degrees) {
    var directions = ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW'];
    return directions[Math.round(degrees / 22.5)];
  }

  getLocationAndWeather();
});
