Build a Random Quote Machine
===
**[Project demo](http://codepen.io/marvinyan/full/eNaxew/)**

[Link to FreeCodeCamp challenge page](http://freecodecamp.com/challenges/zipline-build-a-random-quote-machine)

Objective
---

Build a CodePen.io app that successfully reverse-engineers [this](http://codepen.io/AdventureBear/full/vEoVMw).

User Story: As a user, I can click a button to show me a new random quote.

External resources
---

Quotes API from [Forismatic](http://forismatic.com/en/).
