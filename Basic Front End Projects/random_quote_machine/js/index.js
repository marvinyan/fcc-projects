$(document).ready(function() {
  function generateQuote() {
    // Call quote API
    $.ajax({
      dataType: 'jsonp',
      url: 'http://api.forismatic.com/api/1.0/?method=getQuote&format=jsonp&lang=en&jsonp=?',
      success: function(data) {
        $('#quote').text(data.quoteText);
        if (!data.quoteAuthor.length) {
          $('#author').text("— Unknown");
        } else {
          $('#author').text("— " + data.quoteAuthor);
        }
      }
    });
  }

  // Show initial quote
  generateQuote();

  // Set listener on button
  $('#new-quote-button').click(function() {
    generateQuote();
  });
});
