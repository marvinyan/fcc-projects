$(document).ready(function() {
  // Change values to 0.1 for 6 second timers (debugging)
  // Values are in minutes
  var breakLength = 5;
  var sessionLength = 25;
  var seconds;

  var isPaused = false;
  var isBreak = false;
  var isStarted = false;

  var intervalId;

  // Audio to be played at 0:00
  var audio = new Audio('https://raw.githubusercontent.com/marvinyan/fcc-projects/master/Basic%20Front%20End%20Projects/pomodoro_timer/assets/elevator_ding_sound_effect.mp3');

  function startTimer() {
    $('#timer-status').text('Session');
    clearInterval(intervalId);

    intervalId = setInterval(function() {
      if (!seconds) {
        audio.play();
        startBreak();
      }
      seconds--;
      updateTimerDisplay();
    }, 1000);
  }

  function startBreak() {
    $('#timer-status').text('Break');
    clearInterval(intervalId);

    isBreak = true;
    seconds = breakLength * 60;

    intervalId = setInterval(function() {
      if (!seconds) {
        seconds = sessionLength * 60;
        audio.play();
        startTimer();
      }
      seconds--;
      updateTimerDisplay();
    }, 1000);
  }

  function updateTimerDisplay() {
    $('#timer-time').text(secondsToTime(seconds));
  }

  function updateControlsDisplay() {
    $('#break-value').text(breakLength);
    $('#session-value').text(sessionLength);
    $('#timer-time').text(sessionLength);
  }

  // Assumes seconds < 3600
  function secondsToTime(seconds) {
    var display = Math.floor(seconds / 60) + ':';
    if (seconds % 60 < 10) {
      display += '0';
    }
    display += (seconds % 60);
    return display;
  }

  // Pause if interval is active, resume otherwise
  // Do nothing during break.
  $('.timer-area').click(function() {
    if (!isStarted) { // Click will start the timer
      resetTimer();
      startTimer();
      isStarted = true;
    } else if (!isPaused && !isBreak) { // Click will pause the timer
      clearInterval(intervalId);
      $('#timer-status').text('Paused');
      isPaused = true;
    } else if (isPaused) { // Click will resume timer from pause
      // startTimer();
      isPaused = false;
      startTimer();
    }
  });

  $('.controls').click(function() {
    if (isPaused || !isStarted) {
      if (isPaused) {
        $('#timer-status').text('Session');
      }

      var idName = $(this).attr('id');

      switch (idName) {
        case 'break-minus':
          if (breakLength > 1) {
            breakLength--;
          }
          break;
        case 'break-plus':
          if (breakLength < 60) {
            breakLength++;
          }
          break;
        case 'session-minus':
          if (sessionLength > 1) {
            sessionLength--;
          }
          break;
        case 'session-plus':
          if (sessionLength < 60) {
            sessionLength++;
          }
          break;
      }

      updateControlsDisplay();

      if (isPaused) {
        resetTimer();
      }
    }
  });

  function resetTimer() {
    seconds = sessionLength * 60;
    isPaused = false;
    isBreak = false;
    isStarted = false;
  }
});
