Build a Pomodoro Clock
===
**[Project demo](http://codepen.io/marvinyan/full/mJZeKQ/)**

[Link to FreeCodeCamp challenge page](http://freecodecamp.com/challenges/zipline-build-a-pomodoro-clock)

Objective
---

Build a CodePen.io app that successfully reverse-engineers [this](http://codepen.io/GeoffStorbeck/full/RPbGxZ/).

User Story: As a user, I can start a 25 minute pomodoro, and the timer will go off once 25 minutes has elapsed.

Bonus User Story: As a user, I can reset the clock for my next pomodoro.

Bonus User Story: As a user, I can customize the length of each pomodoro.

Usage
---
[Description of the Pomodoro Technique](https://en.wikipedia.org/wiki/Pomodoro_Technique)

- Click anywhere inside the circle to start a session.
- Once the session has started, click again to pause.
- Settings can be changed only while paused or before a session has started. Doing so will end the current session.
- An audio will play once a session or break ends.
