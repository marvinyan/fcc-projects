$(document).ready(function() {
  var entry = '';
  var stack = [];

  // Button press style effects
  $('button').mouseup(function() {
    $(this).css('border', '2px outset white');
  }).mouseleave(function() {
    $(this).css('border', '2px outset white');
  }).mousedown(function() {
    $(this).css('border', '2px inset white');
  });

  // Clear button
  $('#clear').click(function() {
    entry = '';
    stack = [];
    $('#screen').val(0);
  });

  /* When a number is pressed:
  1) Determine if we are still using a previously calculated result.
  2) Prevent extra '.' from being added.
  3) Concat digits as string and store in entry var.
  4) Show number on screen.
   */
  $('.numbers').click(function() {
    if (stack.length == 1) {
      stack.pop();
    }

    var num = $(this).text();
    if (num === '.' && entry.indexOf('.') !== -1) {
      return;
    }
    entry += $(this).text();
    $('#screen').val(entry);
  });

  /* When an operation is pressed:
  1) Push entry if it is not empty.
  2) Push operation if the last element is a number.
  3) Pop, then push operation if the last element is an operation.
  */
  $('.operations').click(function() {
    if (entry !== '') {
      stack.push(entry);
      entry = '';
    }

    var op = $(this).attr('value');

    // true if last element is an operator
    if (isNaN(stack[stack.length - 1])) {
      stack.pop();
    }

    stack.push(op)
  });

  /* When '=' is pressed:
  1) Push entry, if not blank
  2) Pop last stack element if it is an operator.
  3) Join stack and eval();
  */
  $('#equals').click(function() {
    if (entry !== '') {
      stack.push(entry);
      entry = '';
    }

    if (isNaN(stack[stack.length - 1])) {
      stack.pop();
    }

    var mathChain = stack.join('');
    var ans = eval(mathChain);
    $('#screen').val(ans);

    // Push entry to stack
    stack = [ans];
  });
});
