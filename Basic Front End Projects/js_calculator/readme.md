Build a JavaScript Calculator
===
**[Project demo](http://codepen.io/marvinyan/full/vLKeZr)**

[Link to FreeCodeCamp challenge page](http://www.freecodecamp.com/challenges/zipline-build-a-javascript-calculator)

Objective
---

Build a CodePen.io app that is functionally similar to [this](http://codepen.io/GeoffStorbeck/full/zxgaqw).

User Story: I can add, subtract, multiply and divide two numbers.

User Story: I can clear the input field with a clear button.

User Story: I can keep chaining mathematical operations together until I hit the equal button, and the calculator will tell me the correct output.
