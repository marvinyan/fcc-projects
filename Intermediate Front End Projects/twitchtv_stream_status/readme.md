Use the Twitch.tv JSON API
===
**[Project demo](http://codepen.io/marvinyan/full/MwMVEw/)**

[Link to FreeCodeCamp challenge page](http://freecodecamp.com/challenges/zipline-use-the-twitchtv-json-api)

Objective
---

Build a CodePen.io app that successfully reverse-engineers [this](http://codepen.io/GeoffStorbeck/full/GJKRxZ).

User Story: As a user, I can see whether Free Code Camp is currently streaming on Twitch.tv.

User Story: As a user, I can click the status output and be sent directly to the Free Code Camp's Twitch.tv channel.

User Story: As a user, if Free Code Camp is streaming, I can see additional details about what they are streaming.

External resources
---

Twitch API from [Twitch.tv](https://api.twitch.tv/kraken/).
