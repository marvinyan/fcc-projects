$(document).ready(function() {
  $('#menu-tabs a').click(function(e) {
    e.preventDefault();
    $(this).tab('show');
  });

  var twitchUsernames = ['freecodecamp', 'medrybw', 'storbeck', 'terakilobyte', 'monstercat', 'habathcx', 'RobotCaleb', 'thomasballinger', 'noobs2ninjas', 'beohoff'];
  var userStatus = [];

  function getTwitchJson(usernames, cb) {
    usernames.forEach(function(username) {
      // Get stream info (excluding online/offline)
      $.ajax({
        url: 'https://api.twitch.tv/kraken/channels/' + username + '?client_id=734n4vpo4yagdajjfsq03e8tuhidooq',
        dataType: 'jsonp',
        success: function(data) {
          var userData = {};
          userData.name = data.display_name;
          if (data.logo) {
            userData.logo = data.logo;
          } else {
            userData.logo = 'https://placeholdit.imgix.net/~text?txtsize=7&txt=30%C3%9730&w=30&h=30';
          }
          userData.status = data.status;
          userData.url = data.url;

          // Check if stream is online
          $.ajax({
            url: 'https://api.twitch.tv/kraken/streams/' + username + '?client_id=734n4vpo4yagdajjfsq03e8tuhidooq',
            dataType: 'jsonp',
            success: function(data) {
              if (data.stream !== null) {
                userData.online = true;
              } else {
                userData.online = false;
              }

              userStatus.push(userData);

              // Run callback when all usernames have been checked
              if (userStatus.length === usernames.length) {
                cb();
              }
            }
          })
        }
      });
    });
  }

  // Append list elements to tab-content area
  function updateDisplay() {
    userStatus.forEach(function(user) {
      var logoAndName = '<li class="list-group-item"><a class="nostyle" target="_blank" href="' + user.url + '"><img src="' + user.logo + '"></img>' + user.name + '</a>';

      if (user.online) {
        logoAndName += '<span class="status">' + user.status + '</span></li>';
        $('#online').append(logoAndName);
      } else {
        logoAndName += '</li>';
        $('#offline').append(logoAndName);
      }
      $('#all').append(logoAndName);

    });
  }

  getTwitchJson(twitchUsernames, updateDisplay);
});
