$(document).ready(function() {
  $.getJSON("http://www.freecodecamp.com/news/hot", function(json) {
    json.forEach(function(article) {
      // Picture
      var picture = "<a href='" + article.link + "'><img src='" + article.author.picture + "'></a>";

      // Title
      var headline = article.headline;
      var title = "<div id='title'><a href='" + article.link + "'>" + headline.substr(0, 20);
      if (headline.length > 20) {
        title += "...";
      }
      title += "</a></div>";

      // Author
      var user = article.author.username;
      var author = "<div id='author'>by <a href='http://www.freecodecamp.com/" + user + "'>" + user + "</a></div>";
      
      // Likes count
      var likes = "<div id='likes'>" + article.upVotes.length + " ♥</div>";

      // Date
      var date = new Date(article.timePosted);
      var time = "<div id='date'>Posted " + $.timeago(date) + "</div>";
      
      // The text below the image
      var details = "<div id='details'>" + title + author + likes + time + "</div>";
      
      // Image + text for one article
      var post = "<div class='post'>" + picture + details + "</div>";

      $(".newspage").append(post);
    });
  });
});