Stylize Stories on Camper News
===
**[Project demo](http://codepen.io/marvinyan/full/vLXmWR/)**

[Link to FreeCodeCamp challenge page](http://www.freecodecamp.com/challenges/zipline-stylize-stories-on-camper-news)

Objective
---

Build a CodePen.io app that is functionally similar to [this](http://codepen.io/MarufSarker/full/ZGPZLq/).

User Story: I can browse recent posts from Camper News.

User Story: I can click on a post to be taken to the story's original URL.

User Story: I can see how many upvotes each story has.

External resources
---

[Camper News API](http://www.freecodecamp.com/news/hot)
