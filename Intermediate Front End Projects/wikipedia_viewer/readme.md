Build a Wikipedia Viewer
===
**[Project demo](http://codepen.io/marvinyan/full/xZEPNm)**

[Link to FreeCodeCamp challenge page](http://www.freecodecamp.com/challenges/zipline-build-a-wikipedia-viewer)

Objective
---

Build a CodePen.io app that is functionally similar to [this](http://codepen.io/GeoffStorbeck/full/MwgQea).

User Story: I can search Wikipedia entries in a search box and see the resulting Wikipedia entries.

User Story: I can click a button to see a random Wikipedia entry.

User Story: When I type in the search box, I can see a dropdown menu with autocomplete options for matching Wikipedia entries.

Usage
---

- Type any query into the searchbox to see a list of autocomplete entries.
- Click on an autocomplete entry to open that article in a new window.
- Click the magnifying glass to see additional details for each autocomplete entry.

External resources
---

[Wikipedia API](https://en.wikipedia.org/w/api.php)
