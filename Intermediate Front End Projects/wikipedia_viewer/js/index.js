$(document).ready(function() {
  var articles = null;

  $("#searchBox").autocomplete({
    // request.term contains the search text
    // data[1]: titles
    // data[2]: descriptions
    // data[3]: links
    source: function(request, response) {
      $.ajax({
        url: "https://en.wikipedia.org/w/api.php",
        dataType: "jsonp",
        data: {
          format: "json",
          action: "opensearch",
          search: request.term
        },
        success: function(data) {
          response(data[1]);
          articles = data;
        }
      })
    },
    autoFocus: true,
    select: function(event, ui) {
      window.open("https://en.wikipedia.org/wiki/" + ui.item.value);
    }
  });

  $("#searchButton").click(function() {
    if ($("#searchBox").val().length > 0 && articles !== null) {
      $("#results").empty();
      $("#random").remove();

      for (var i = 0; i < articles[1].length; i++) {
        var result = "<div class='result'>";
        result += "<a target='_blank' href='" + articles[3][i] + "'>"
          + "<h4>" + articles[1][i] + "</h4>"
          + "<p>" + articles[2][i] + "</p></a>";
        $("#results").append(result);
      };
    }
  });
});
